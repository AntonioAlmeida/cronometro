﻿

namespace cronometro
{
    using System;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {

        private readonly Cronometro _cronometro;

        public Form1()
        {
            InitializeComponent();
            _cronometro = new Cronometro();
        }

        private void BtnOnOff_Click(object sender, EventArgs e)
        {
            if (_cronometro.ClockState())
            {

                _cronometro.StopClock();
                BtnOnOff.Text = "Liga";
                TimerRelogio.Enabled = false;
                // LblContador.Text = _cronometro.GetTimeSpan().ToString();



            }
            else
            {
                _cronometro.StartClock();
                BtnOnOff.Text = "Desliga";
                TimerRelogio.Enabled = true;
            }
        }
        private void UpdateLabel()
        {
            var tempo = DateTime.Now - _cronometro.StartTime();
            LblContador.Text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", tempo.Hours, tempo.Minutes, tempo.Seconds,tempo.Milliseconds);
        }

        private void TimerRelogio_Tick(object sender, EventArgs e)
        {
            UpdateLabel();
        }
    }
}

