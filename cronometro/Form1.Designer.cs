﻿namespace cronometro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BtnOnOff = new System.Windows.Forms.Button();
            this.LblContador = new System.Windows.Forms.Label();
            this.TimerRelogio = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // BtnOnOff
            // 
            this.BtnOnOff.Location = new System.Drawing.Point(111, 39);
            this.BtnOnOff.Name = "BtnOnOff";
            this.BtnOnOff.Size = new System.Drawing.Size(75, 23);
            this.BtnOnOff.TabIndex = 0;
            this.BtnOnOff.Text = "Ligar/Desligar";
            this.BtnOnOff.UseVisualStyleBackColor = true;
            this.BtnOnOff.Click += new System.EventHandler(this.BtnOnOff_Click);
            // 
            // LblContador
            // 
            this.LblContador.AutoSize = true;
            this.LblContador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblContador.Location = new System.Drawing.Point(96, 102);
            this.LblContador.Name = "LblContador";
            this.LblContador.Size = new System.Drawing.Size(102, 20);
            this.LblContador.TabIndex = 1;
            this.LblContador.Text = "00:00:00:000";
            // 
            // TimerRelogio
            // 
            this.TimerRelogio.Tick += new System.EventHandler(this.TimerRelogio_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.LblContador);
            this.Controls.Add(this.BtnOnOff);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnOnOff;
        private System.Windows.Forms.Label LblContador;
        private System.Windows.Forms.Timer TimerRelogio;
    }
}

