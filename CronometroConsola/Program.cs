﻿

namespace CronometroConsola
{
    using cronometro;
    using System;
    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();
            Console.WriteLine("Pressione Enter para Iniciar o Cronometro");
                Console.ReadLine();

            relogio.StartClock();
            Console.WriteLine("Pressione Enter para Iniciar o Cronometro");

            while (relogio.ClockState())
            {
                var tempo = DateTime.Now - relogio.StartTime();
                Console.Write("\r Tempo corrente:{0}", tempo);

                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            }
            relogio.StopClock();

            Console.WriteLine("\r O Tempo Cronometrado: {0}", relogio.GetTimeSpan());
            Console.ReadKey();
        }
    }
}
